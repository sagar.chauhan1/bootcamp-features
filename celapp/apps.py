from django.apps import AppConfig


class CelappConfig(AppConfig):
    name = 'celapp'
