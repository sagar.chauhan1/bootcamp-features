from bootcamp.qa.models import Question
from bootcamp.users.models import User
from bootcamp.celery import app
from celery import shared_task, task
import json
import os


@app.task
def create_json(User_id):
    qustions = Question.objects.filter(user=User_id)
    print(qustions)
    name = User.objects.get(pk=User_id)
    print("dsvfbgh =    ", name.get_profile_name())
    file = name.get_profile_name()+".json"
    path = os.path.join(os.environ.get('HOME'), file)
    #path = os.path.join('/home/ubuntu/bootcamp-features/downloads/', file)
    with open(path, 'w') as outfile:
        i = 0
        content = {}
        for question in qustions:
            content[i] = {'title': question.title,
                          'timestamp': question.timestamp.isoformat(),
                          'content': question.content
                          }
            i += 1
        print(content)
        json.dump(content, outfile)
    print("file created")
