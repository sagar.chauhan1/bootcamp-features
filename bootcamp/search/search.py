from bootcamp.qa import models
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Object, Text, Date, Search
#connections.create_connection()
connections.create_connection(hosts=['https://elastic:cO8iM9YSJCZi8N0zTqcqgX2G@9f2d2a44285348c1bb802b1ddef147f2.us-east-1.aws.found.io:9243'])

class QuestionIndex(DocType):
    user = Text()
    title = Text()
    timestamp = Date()
    content = Text()
    id = Text()

    class Meta:
        index = 'question-index'


def bulk_indexing():
    # QuestionIndex.init()
    es = Elasticsearch()
    bulk(client=es, actions=(b.indexing()
                             for b in models.Question.objects.all().iterator()))


def search(query):
    s = Search(index='question-index').filter("query_string",
                                              query='*'+query+'*', fields=['title', 'content', 'tags'])
    QuestionQuerySet = s.execute()
    ids = [query["id"] for query in QuestionQuerySet]
    return ids
