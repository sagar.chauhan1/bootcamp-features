# Use an official Python runtime as a parent image
FROM python:3.6-slim

# Set the working directory to /app
WORKDIR ./bootcamp


# Copy the current directory contents into the container at /app
COPY . /usr/src/bootcamp

# Install any needed packages specified in requirements.txt
RUN apt-get update
#RUN apt-get install -y --no-install-recommends apt-utils
#RUN apt-get install -y postgresql-client-9.6
#RUN apt-get install -y postgresql 
#RUN apt-get install -y postgresql-contrib
#RUN apt-get install postgresql-server-dev-9.5
RUN apt-get install -y gcc
RUN apt-get install -y libpq-dev python3-dev
RUN pip install psycopg2
#RUN pwd
#WORKDIR cd /home/ubuntu/bootcamp-features
#RUN pwd
#RUN apt-get install -y python3-pip
#RUN pwd
#RUN pip3 install -r local.txt
#RUN pip3 install -r base.txt
RUN pip3 install -r 'req.txt'
RUN pip3 install -r base.txt
RUN pip3 install -r requirements/local.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run app.py when the container launches
CMD ["python", "app.py"]                                                    
